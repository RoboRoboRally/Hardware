
// Configuration for controller board.
#include "config/board.cnc-shield-v3.h"

// Configuration for printer board.
#include "config/plotter.robo.h"
