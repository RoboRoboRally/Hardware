$srcDir = "scad"
$outDir = "stl"

function compile( [string]$source, [string]$target, [string]$options = '')
{
	echo "Compiling $target";
	openscad $options -o "$outDir/$target.stl" "$srcDir/$source.scad"
}

mkdir -Force $outDir | Out-Null
compile "Leg_Base" "Leg_Base"

compile "Leg_Hook" "Leg_Hook"
compile "Pedestal" "Pedestal"
compile "Z_axis_adapter" "Z_axis_adapter"
compile "axis" "axis"