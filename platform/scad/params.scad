$fn = 60;
// screw M4
screwRadius = 4.6 / 2; //just some lee-way
screwHoleLengthShort = 15;
screwHoleLengthLong = 25;

nutSideCount = 6;
nutRadius = 8 / 2; //standard M4 nut diameter
nutHeight = 3.3; // standard M4 nut height is between 2.9 and 3.2 mm

nutHoleHeight = 10;
nutHoleDepth = 4.5;
nutHoleDistanceFromEdge = 5;

mainPartWidth = 200;
mainPartDepth = 200;
mainPartHeight = 10;

centralMainPartWidth = 170;
centralMainPartDepth = 180;
centralMainPartHeight = 2;

gameBoardHeight = 2;
gameBoardWidth = 250;
gameBoardBaseDepth = 250;
starterBoardWidth = 250;
starterBoardDepth = 80;
gameBoardDepth = gameBoardBaseDepth + starterBoardDepth;
gameBoardDelimiterSize = 1;

sideConnectorDistance = 20;
sideConnectorLength = 80;
sideConnectorThickness = 10;

legConnectorPartLength = 35;
legConnectorStrengthenerThickness = 2;
legConnectorStrengthenerLength = 15;
legConnectorShortWidth = 10;
legConnectorWideWidth = 34;
legConnectorScrewDistance = 10;