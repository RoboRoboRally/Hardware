use <lib/MCAD/involute_gears.scad>
use <common.scad>

$fn = 60;



teethCount = 16;
gear_circular_pitch = 79;
gear_pressure_angle = 30;
gear_clearance = 0;
gear_hub_diameter = 0;
gear_bore_diameter = 0;
gear_backlash = 0;

holder_radius = 16;
axis_adapter_height = 5;
reserve = 1;
magnet_hole_height_main = 3.25;
magnet_hole_height_secondary = 2.25;
magnet_hole_radius = 5.25;

main_magnet_offset = magnet_hole_radius + 0.25;
secondary_magnet_offset = 9;

screwHole_radius = 1;
screwHole_depth = 10;

wing_length = 10;
wing_width = 4;

module axis_adapter()
{
    difference()
    {
        cylinder(r = holder_radius, h = axis_adapter_height);

        gear(
            number_of_teeth=teethCount,
            circular_pitch=gear_circular_pitch, 
            pressure_angle=gear_pressure_angle,
            clearance = gear_clearance,
            rim_thickness=axis_adapter_height,
            hub_diameter=gear_hub_diameter,
            bore_diameter=gear_bore_diameter,
            backlash=gear_backlash);
    }
}

module magnet_holder()
{
    difference()
    {
        cylinder(r = holder_radius, h = magnet_hole_height_main);
        
        translate([main_magnet_offset, 0, 0])
            cylinder(r = magnet_hole_radius, h = magnet_hole_height_main);
        
        translate([-main_magnet_offset, 0, 0])
            cylinder(r = magnet_hole_radius, h = magnet_hole_height_main);
        
        h_offset = magnet_hole_height_main - magnet_hole_height_secondary;
        translate([0, secondary_magnet_offset, h_offset])
            cylinder(r = magnet_hole_radius, h = magnet_hole_height_secondary);
        
        translate([0, -secondary_magnet_offset, h_offset])
            cylinder(r = magnet_hole_radius, h = magnet_hole_height_secondary);
    }
}

module wing()
{
    translate([holder_radius - 1, -wing_width / 2, 0])
    {
        cube([wing_length, wing_width, axis_adapter_height]);
        translate([wing_length, wing_width / 2, 0])
            cylinder(r = wing_width / 2, h = axis_adapter_height);
    }    
}

module holder()
{
    translate([0, 0, -axis_adapter_height])
    {
        axis_adapter();
        wing();
    }    

    cylinder(r = holder_radius, h = reserve);

    translate([0, 0, reserve])
    {
        magnet_holder();
    }   
}



difference()
{
    holder();
    
    cylinder(r = screwHole_radius, h = screwHole_depth);
}


    


