cover_radius = 15;
cover_height = 0.5;

$fn = 60;


cylinder(r = cover_radius, h = cover_height);