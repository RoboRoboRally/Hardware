include <params.scad>
use <common.scad>

$fn = 60;

axis_hole_radius = 7;
screw_holes_horizontal_distance = 16;
screw_holes_vertical_distance = 16;
axis_hole_closer_screw_hole_horizontal_distance = 41;
height = 6;

padding_left = axis_hole_radius + 4;
padding_right = 4;
padding_top = 5;
padding_bottom = 5;

width = padding_left + axis_hole_closer_screw_hole_horizontal_distance + screw_holes_horizontal_distance + padding_right;
depth = padding_top + screw_holes_vertical_distance + padding_bottom;

screw_hole_length = 30;
nut_hole_offset = 20;

padding_botttom_from_axis_hole = screw_holes_horizontal_distance / 2 - axis_hole_radius + padding_bottom;
padding_top_from_axis_hole = screw_holes_horizontal_distance / 2 - axis_hole_radius + padding_top;
split_point = padding_left; 


module placed_screwHole(x, y)
{
    translate([x, y, height])
        rotate(a = 90, v = [0, 1, 0])
            screwHole(height);
}

module placed_screwHoleWithNutHole(y)
{
    translate([0, y, height / 2])
    {       
        screwHole(screw_hole_length);
        
        translate([nutHoleDepth / 2 + nut_hole_offset, 
                    0, 0])
            cube([nutHoleDepth, nutHoleHeight, height], center=true);
        
    }    
}

module z_axis_holder()
{
	difference()
	{
		cube([width, depth, height]);
		
		translate([padding_left, 
				padding_bottom +  screw_holes_vertical_distance / 2, 0])
			cylinder(r = axis_hole_radius, h = height);
		
		placed_screwHole(width - padding_right, padding_bottom);
		placed_screwHole(width - padding_right, depth - padding_top);
		placed_screwHole(width - padding_right - screw_holes_horizontal_distance, padding_bottom);
		placed_screwHole(width - padding_right - screw_holes_horizontal_distance, depth - padding_top);
		
		placed_screwHoleWithNutHole(padding_botttom_from_axis_hole / 2);
		
		placed_screwHoleWithNutHole(depth - padding_top_from_axis_hole / 2);
	}
}

module z_axis_holder_part1()
{
	difference()
	{
		z_axis_holder();

		translate([split_point, 0, 0])
			cube([width, depth, height]);
	}
}

module z_axis_holder_part2()
{
	difference()
	{
		z_axis_holder();

		translate([-width + split_point, 0, 0])
			cube([width, depth, height]);
	}
}

z_axis_holder_part1();
//z_axis_holder_part2();
