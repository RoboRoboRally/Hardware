include <common.scad>

legHeight = 170;
legWidth = 10;
legDepth = 10;

screwHoleBottomStart = screwRadius * 2;
screwHoleStep = 8;
screwHoleBottomCount = 3;

screwHoleTopStart = 165;
screwHoleTopCount = 5;

module movedScrewHole(height){
	translate([0, legDepth / 2, height])
        screwHole(legWidth);
}

difference(){
	cube([legDepth, legWidth, legHeight]);
	
	for (i = [0:screwHoleBottomCount - 1])
		movedScrewHole(screwHoleBottomStart + i * screwHoleStep);
	
	for(i = [0:screwHoleTopCount - 1])
		movedScrewHole(screwHoleTopStart - i * screwHoleStep);
}