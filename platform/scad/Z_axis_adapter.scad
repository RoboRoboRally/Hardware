use <lib/MCAD/involute_gears.scad>
use <common.scad>

$fn = 60;

innerCylinderRadius = 6;
innerCylinderHeight = 6;

teethCount = 17;
gear_circular_pitch = 100;
gear_pressure_angle = 30;
gear_clearance = 0;
gear_hub_diameter = 0;
gear_bore_diameter = 0;
gear_backlash = 0;

outer_gear_circular_pitch = 200;
outer_gear_height = 18;

wall_height = 4;
inner_gear_side_wall_width = 4;
other_side_wall_width = 2;

wall_radius = 8 + wall_height;

difference()
{
    gear(
    number_of_teeth=teethCount,
	circular_pitch=outer_gear_circular_pitch, 
	pressure_angle=gear_pressure_angle,
	clearance = gear_clearance,
	rim_thickness=outer_gear_height,
	hub_diameter=gear_hub_diameter,
	bore_diameter=gear_bore_diameter,
	backlash=gear_backlash);

    gear(
    number_of_teeth=teethCount,
	circular_pitch=gear_circular_pitch, 
	pressure_angle=gear_pressure_angle,
	clearance = gear_clearance,
	rim_thickness=innerCylinderHeight,
	hub_diameter=gear_hub_diameter,
	bore_diameter=gear_bore_diameter,
	backlash=gear_backlash);
}

module side(wall_width)
{
    difference()
    {
        cylinder(h = wall_width, 
                r = wall_radius);
        
        cylinder(h = wall_width,
                r = wall_radius - wall_height);
    }
}

side(inner_gear_side_wall_width);
translate([0, 0, 
        outer_gear_height - other_side_wall_width])
    side(other_side_wall_width);