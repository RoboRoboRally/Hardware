use <common.scad>
use <lib/Timing_belt/timing_belt.scad>
$fn = 100;

engine_height = 15;
engine_radius = 11;

engine_holder_thickness = 1.5;

engine_holder_total_radius = engine_radius + engine_holder_thickness;
bolt_holder_length = 10;
bolt_holder_height = 8;
bolt_holder_radius = 3.5;
bolt_distance = 2;

screw_radius = 1.2;
screw_extra_thingy_radius = screw_radius + 1;
screw_extra_thingy_height = 2;

home_holder_thickness = 3;
home_holder_height = 95;

connector_width = 12;
connector_angle = 205;

axis_radius = 6;
total_height = 170;
axis_height = total_height 
            - engine_holder_thickness 
            - engine_height 
            - engine_radius;
            
belt_length = 40;
belt_width = 10;
axis_bottom_radius = 8.5;



engine_holder();

translate([0, 0, -axis_height + belt_length])
    sphere(axis_bottom_radius);

translate([0, 0, -axis_height])
{
    cylinder(r = axis_radius, h = axis_height);
    axis_bottom();
}

translate([0, -axis_radius + home_holder_thickness, -axis_height + home_holder_height])
        home_holder();




module engine_holder_wing()
{
    difference()
    {
        union()
        {
            difference()
            {
                translate([bolt_holder_radius, 0, 0])
                    perpendicularTriplePrism(
                        -engine_holder_total_radius -  bolt_holder_radius, 
                        engine_holder_total_radius + bolt_holder_length + 2.4,
                        bolt_holder_height);
                
                translate([-engine_radius / 2 + 3, 
                        engine_radius + bolt_holder_radius - 1, 
                        0])
                    cube([engine_radius * 2, 
                        engine_radius * 2, 
                        bolt_holder_height]);        
            }
    
            translate([0, engine_radius + bolt_distance, 0])
                cylinder(r = bolt_holder_radius, h = bolt_holder_height);
        }
    }
}

module engine_holder_screwhole()
{
    //screw hole thingy around
    translate([0, 0, bolt_holder_height - screw_extra_thingy_height])
        cylinder(r = screw_extra_thingy_radius, 
                h = screw_extra_thingy_height);
    //screw hole
    cylinder(r = screw_radius, h = bolt_holder_height);
}

module engine_holder()
{
    translate([0, 0, engine_radius])
        difference()
        {
            union()
            {
                translate([0, 0, -engine_radius])
                    cylinder(r = engine_holder_total_radius, 
                            h = engine_height 
                                + engine_holder_thickness 
                                + engine_radius);
                
                translate([0, 0, engine_height + engine_holder_thickness -      bolt_holder_height])
                    engine_holder_wing();
                
                translate([0, 0, engine_height + engine_holder_thickness -      bolt_holder_height])
                    mirror([0, 1, 0])
                        engine_holder_wing();   
            }
            
            sphere(engine_radius);
            
            translate([0, 0, engine_holder_thickness])
                cylinder(r = engine_radius, h = engine_height);    
            
            // hole for connector
            rotate(v = [0, 0, 1], a = connector_angle)
            translate([engine_radius, 0, engine_height / 2 + engine_holder_thickness])
                cube([engine_holder_thickness * 3, 
                        connector_width,
                        engine_height], 
                    center=true);
            
            // hole for air breathing
            rotate(v = [0, 0, 1], a = connector_angle + 180)
            translate([engine_radius, 0, engine_height / 2 - engine_holder_thickness])
                cube([engine_holder_thickness * 3, 
                        connector_width,
                        engine_height / 2], 
                    center=true);
            
            translate([0, 
                    engine_radius + bolt_distance, 
                    engine_height + engine_holder_thickness - bolt_holder_height])
                engine_holder_screwhole();
            
            translate([0, 
                    -(engine_radius + bolt_distance), 
                    engine_height + engine_holder_thickness - bolt_holder_height])
                engine_holder_screwhole();
        }
    
    
}


module axis_bottom()
{
    cylinder(r = axis_bottom_radius, h = belt_length);
    
    translate([0, axis_bottom_radius, 0])
        rotate(v=[0, 1, 0], a = -90)
    {
        scale(1.8)
            belt_len(profile=0, 
                belt_width=belt_width / 1.8,
                len=belt_length / 1.8);
    }
    
    translate([0, -axis_bottom_radius, 0])
    {
        rotate([0, -90, 180])
        {            
            scale(1.8)
                belt_len(profile = 0,
                    belt_width = belt_width / 1.8,
                    len=belt_length / 1.8);
        }
    }            
}

module home_holder()
{
    rotate([90, 0, 0])
        home_holder_standard(home_holder_thickness);
}
