include<common.scad>

head_length = 12;

height = 8;
thickness = 10;
length1 = 110;
length2 = 55 - head_length;

difference()
{
    union()
    {
        cube([length1, thickness, height]);
        
        translate([length1 - thickness, 0, 0])
            cube([thickness, length2, height]);
        
        translate([length1 - thickness / 2, length2 + thickness / 2, 0])
            home_holder_standard(height);
    }
    
    translate([height / 2, 0, height / 2])
        rotate(v = [0, 0, 1], a = 90)
            screwHole(thickness);
}