use <lib/MCAD/involute_gears.scad>
use <common.scad>

$fn = 60;

epsilon = 0.1;
inner_hole_diameter = 4 + epsilon;

teethCount = 17;
gear_circular_pitch = 200;
gear_pressure_angle = 30;
gear_height = 18;
gear_clearance = 0;
gear_hub_diameter = 0;
gear_bore_diameter = 0;
gear_backlash = 0;

wall_width = 2;
wall_height = 4;
wall_radius = 8 + wall_height;

difference () {
    union() {
        gear(
            number_of_teeth=teethCount,
            circular_pitch=gear_circular_pitch, 
            pressure_angle=gear_pressure_angle,
            clearance = gear_clearance,
            rim_thickness=gear_height,
            hub_diameter=gear_hub_diameter,
            bore_diameter=gear_bore_diameter,
            backlash=gear_backlash);
            
        cylinder(r=5, h=gear_height);    
    }
    
    cylinder(r=inner_hole_diameter / 2, h=gear_height);
}

module side(wall_width)
{
    difference()
    {
        cylinder(h = wall_width, 
                r = wall_radius);
        
        cylinder(h = wall_width,
                r = wall_radius - wall_height);
    }
}

side(wall_width);
translate([0, 0, gear_height - wall_width])
    side(wall_width);