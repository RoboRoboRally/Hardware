$fn = 60;

screwHole_radius = 2.25;
depth = 5;
hole_distance = 8;
axis_radius = 4.1 / 2;

engine_side_hole_height = 15;
reserve = [8, 7];

extension_distance_from_engine_to_axis = 67.5;
extension_distance_from_axis_to_screw = 16.5;

engine_side_dimensions = [
    2 * reserve.x + 2 * hole_distance,
    2 * reserve.y + engine_side_hole_height,
    depth
];

extension_dimensions = [
    extension_distance_from_axis_to_screw
        + extension_distance_from_engine_to_axis,
    2 * reserve.y,
    depth
];

module screw_hole() {
    cylinder(r=screwHole_radius, h=depth);
}

module long_hole(distance) {
    hull() {    
        screw_hole();
        
        translate([0, distance, 0]) {
            screw_hole();
        }
    }
}

difference() {
    cube(engine_side_dimensions);
    
    translate([reserve.x, reserve.y, 0])
        long_hole(engine_side_hole_height);    
    
    translate([engine_side_dimensions.x - reserve.x, reserve.y, 0])
        long_hole(engine_side_hole_height);    
}

difference() {
    mirror()
        cube(extension_dimensions);
    
    base_x = reserve.x;    
    
    position_axis_hole = base_x - 
        extension_distance_from_engine_to_axis;
    
    position_screw_hole = position_axis_hole - 
        extension_distance_from_axis_to_screw;
        
    translate([position_axis_hole, reserve.y, 0])
        cylinder(r = axis_radius, h=depth);
    
    translate([position_screw_hole, reserve.y, 0])
        screw_hole();
    
    
    
}