use <lib/threads/threads.scad>

$fn = 100;

diameter = 22.5;
outerRingThickness = 0.75;
height = 6.5;
outerRingHeight = 2.5;

magnetDiameter = 9.25;
magnetHeight = 5.5;
magnetOffset = 5;

arrowTipDistance = 14;
xScale = 1.35;

module arrowTip(radius, diameter_short, height)
{    
    side = (radius * 2) / sqrt(2);
    difference()
    {
        scale([xScale, 1, 1])
        {
            rotate(v = [0, 0, 1], a = 45)
            {
                translate([-side / 2, -side / 2, 0])
                {
                    cube([side, side, 
                        height]);   
                }
            } 
        }
        
        translate([-side, - 2 * side, 0])
            cube([2 * side, 2 * side, height]);
        
        translate([diameter_short, -diameter_short / 2, 0])
            cube([side, side, height]);
        
        translate([-diameter_short - side, -diameter_short / 2, 0])
            cube([side, side, height]);
    }
    
}

difference()
{
    union()
    {
        cylinder(r = diameter / 2, h = height + outerRingHeight);
        arrowTip(arrowTipDistance, diameter / 2, outerRingHeight + height);
    }
    
    
    translate([0, 0, height])
    {
        arrowTip(arrowTipDistance - outerRingThickness, 
                diameter / 2 - outerRingThickness, outerRingHeight);
        cylinder(   r = diameter / 2 - outerRingThickness, 
                    h = outerRingHeight);
    }
    
    translate([-magnetOffset, 0, height - magnetHeight])
        cylinder(r = magnetDiameter / 2, h = magnetHeight);
    
    translate([magnetOffset, 0, height - magnetHeight])
        cylinder(r = magnetDiameter / 2, h = magnetHeight);
}