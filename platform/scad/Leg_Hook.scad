include<common.scad>

Length = 20;
Height = 8;
Width = 10;
HookLength = 10;
HookHeight = 18;

CylinderRadius = 2;

difference()
{
    union()
    {
        cube([Length, Width, Height]);
        cube([HookLength, Width, HookHeight]);
        translate([HookLength, 0, Height])
            cube([CylinderRadius, Width, CylinderRadius]);
    }
    translate([0, Width / 2, Height / 2])
        screwHole(Length);
    
    translate([HookLength + CylinderRadius, 0, Height + CylinderRadius])
        rotate(v = [1, 0, 0], a = -90)
            cylinder(r = CylinderRadius, h = Width);
}