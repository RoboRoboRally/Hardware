$fn = 60;
screwHole_radius = 2.25;
height = 5;

hole_distance_x = 44;
hole_distance_y = 16;

reserve_x = 8;
reserve_y = 7;

gear_axis_distance = 37.5;
gear_axis_hole_radius = 2;

cube_dimensions = [hole_distance_x + 2 * reserve_x, 
        hole_distance_y + 2 * reserve_y,
        height];

module engine_side() {
    difference()
    {
        cube(cube_dimensions);
        
        translate([reserve_x, reserve_y, 0])
            cylinder(r = screwHole_radius, h = height);
        
        translate([reserve_x + hole_distance_x,
            reserve_y, 0])
            cylinder(r = screwHole_radius, h = height);
        
        translate([reserve_x, hole_distance_y + reserve_y, 0])
            cylinder(r = screwHole_radius, h = height);            
    }
}

module extension() {
    axis_hole_point = gear_axis_distance - (cube_dimensions.x / 2);
        
    extension_length = axis_hole_point
        + 2 * gear_axis_hole_radius
        + 2 * screwHole_radius
        + reserve_x * 2;
    
    extension_dimensions = [extension_length,
        2 * reserve_y,
        height];
    
    difference() {
        cube(extension_dimensions);
        
        translate([axis_hole_point, reserve_y, 0])
            cylinder(r = gear_axis_hole_radius, h = height);
        
        translate([extension_dimensions.x - reserve_x, reserve_y, 0])
            cylinder(r = screwHole_radius, h = height);
    }
}

engine_side();
translate([cube_dimensions.x, 0, 0])
    extension();