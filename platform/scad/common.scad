include <params.scad>

module screwHole(length){    
	rotate(a = 90, v=[0, 1, 0])
		cylinder(h = length, r = screwRadius);
}

module nutHole(height){
    cube([height, nutHoleDepth, nutHoleHeight]);
}

module screwAndNutHole(length, height){
	screwHole(length);
    
    translate([ nutHoleDepth + nutHoleDistanceFromEdge,
                -height / 2,
                -nutHoleHeight / 2])
        rotate(a=90, v=[0, 0, 1])
            nutHole(height);
}

module perpendicularTriplePrism(a, b, height)
{
    Points = [
    [0, 0, 0], // 0
    [a, 0, 0], // 1
    [0, b, 0], // 2
    [0, 0, height], // 3
    [a, 0, height], // 4
    [0, b, height]  // 5
    ];
    
    Faces = [
    [0, 1, 2], //bottom
    [0, 3, 4, 1], // a side
    [3, 5, 4], //top    
    [0, 2, 5, 3], // b side
    [1, 4, 5, 2] // c side
    ];
    
    polyhedron(Points, Faces);
}

module screw_long_hole(screw_radius, length, height)
{
    translate([0, 0, height / 2])
        cube([screw_radius * 2, length - 2 * screw_radius, height], 
            center = true);
    
    translate([0, length / 2 - screw_radius, 0])
        cylinder(r = screw_radius, h = height);
    
    translate([0, -(length / 2 - screw_radius), 0])
        cylinder(r = screw_radius, h = height);
}

module home_holder(width, length, height, screw_radius, hole_length, hole_distance)
{
    translate([-width / 2, - length /2])
        difference()
        {
            cube([width, length, height]);
            
            translate([(width - hole_distance) / 2, length / 2, 0])
                screw_long_hole(screw_radius, hole_length, height);
            
            translate([(width + hole_distance) / 2, length / 2, 0])
                screw_long_hole(screw_radius, hole_length, height);
        }
}

module home_holder_standard(height)
{    
    home_holder(
        width = 15,
        length = 12,
        height = height,
        screw_radius = 1,
        hole_length = 10,
        hole_distance = 10
        );
}