Notes for generating the platform files and printing them:

Prerequsities: openscad in PATH

0) (OPTIONAL) modify parameters in the scad/params.scad file
1) Running the compile.ps1 powershell script will generate all the required .stl files in the stl subfolder
2) print:

Count			Part name
4				Leg_Base.stl 
4				Leg_Hook.stl
$PlayerCount	Pedestal.stl